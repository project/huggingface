<?php

namespace Drupal\augmentor_huggingface\Plugin\Augmentor;

use Drupal\augmentor_huggingface\HuggingfaceBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Huggingface Image Classification Model Augmentor plugin implementation.
 *
 * @Augmentor(
 *   id = "huggingface_image_classification",
 *   label = @Translation("Huggingface Image Classification"),
 *   description = @Translation("Uses image classification models to label an image"),
 * )
 */
class HuggingfaceImageClassification extends HuggingfaceBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'max_labels' => NULL,
      'min_score' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['max_labels'] = [
      '#type' => 'number',
      '#title' => $this->t('Max number of labels'),
      '#default_value' => $this->configuration['max_labels'],
      '#description' => $this->t('The maximum number of labels to detect from a given image.'),
    ];

    $form['min_score'] = [
      '#type' => 'number',
      '#step' => '.01',
      '#title' => $this->t('Min score'),
      '#default_value' => $this->configuration['min_score'] ?? 0.7,
      '#description' => $this->t('The confidence score, can differ per model.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['max_labels'] = $form_state->getValue('max_labels');
    $this->configuration['min_score'] = $form_state->getValue('min_score');
  }

  /**
   * Perform image classification on a given image path.
   *
   * @param string $path
   *   The image path to process.
   *
   * @return array
   *   The detected labels.
   */
  public function execute($path) {
    try {
      $response = json_decode($this->api->imageClassification($this->getEndpoint(), $path), TRUE);
      foreach ($response as $result) {
        if ($result['score'] >= $this->configuration['min_score']) {
          $labels[] = $result['label'];
        }
      }
      return ['default' => $labels];
    } catch (\Throwable $error) {
      $this->logger->error('Huggingface Fetch Error: %message.', [
        '%message' => $error->getMessage(),
      ]);
      return [
        '_errors' => $this->t('Error during the labels detection, please check the logs for more information.')->render(),
      ];
    }
  }

}

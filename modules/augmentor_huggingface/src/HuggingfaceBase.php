<?php

namespace Drupal\augmentor_huggingface;

use Drupal\augmentor\AugmentorBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\huggingface\HuggingfaceApi;
use Drupal\key\KeyRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Huggingface base to extend.
 */
class HuggingfaceBase extends AugmentorBase implements ContainerFactoryPluginInterface {

  /**
   * The huggingface api.
   *
   * @var \Drupal\huggingface\HuggingfaceApi
   */
  protected $api;

  /**
   * Construct a Huggingface augmentor field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\huggingface\HuggingfaceApi $api
   *   The huggingface api.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    HuggingfaceApi $api,
    LoggerInterface $logger,
    KeyRepositoryInterface $key_repository,
    AccountInterface $current_user,
    FileSystemInterface $file_system,
    FileRepositoryInterface $file_repository
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $logger,
      $key_repository,
      $current_user,
      $file_system,
      $file_repository
    );
    $this->api = $api;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('huggingface.api'),
      $container->get('logger.factory')->get('augmentor'),
      $container->get('key.repository'),
      $container->get('current_user'),
      $container->get('file_system'),
      $container->get('file.repository'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'model' => NULL,
      'inference_type' => 'api',
      'url' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Key is set in the Huggingface core model.
    unset($form['key']);

    // Inference type.
    $form['inference_type'] = [
      '#type' => 'select',
      '#title' => 'Type of inference',
      '#description' => $this->t('Choose the type of inference to use. Note that the API is rate limited, even if its free and note that some models does not allow both types.'),
      '#options' => [
        'api' => $this->t('API Inference (free)'),
        'dedicated' => $this->t('Dedicated Inference (paid)'),
      ],
      '#default_value' => $this->configuration['inference_type'] ?? 'api',
    ];

    // If API, choose the model.
    $form['model'] = [
      '#type' => 'textfield',
      '#title' => 'Huggingface Model',
      '#description' => $this->t('The full namespace to the image classification model. For instance dima806/facial_emotions_image_detection'),
      '#default_value' => $this->configuration['model'] ?? '',
      '#autocomplete_route_name' => 'huggingface.autocomplete.models',
      '#autocomplete_route_parameters' => [
        'model_type' => 'image-classification',
      ],
      '#states' => [
        'visible' => [
          ':input[name="settings[inference_type]"]' => [
            'value' => 'api',
          ],
        ],
      ],
    ];

    // Otherwise dedicated URL.
    $form['interpolator_huggingface_url'] = [
      '#type' => 'textfield',
      '#title' => 'Huggingface Endpoint URL',
      '#description' => $this->t('The full endpoint URL to the dedicated service.'),
      '#default_value' => $this->configuration['url'] ?? '',
      '#states' => [
        'visible' => [
          ':input[name="settings[inference_type]"]' => [
            'value' => 'dedicated',
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if ($form_state->getValue('inference_type') === 'api' && empty($form_state->getValue('model'))) {
      $form_state->setErrorByName('model', $this->t('The model is required when using the API.'));
    }
    if ($form_state->getValue('inference_type') === 'dedicated' && empty($form_state->getValue('url'))) {
      $form_state->setErrorByName('url', $this->t('The url is required when using a dedicated inference.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['inference_type'] = $form_state->getValue('inference_type');
    $this->configuration['model'] = $form_state->getValue('model');
    $this->configuration['url'] = $form_state->getValue('url');
  }

  /**
   * Get the endpoint.
   *
   * @return string
   *   The endpoint.
   */
  protected function getEndpoint() {
    return $this->configuration['inference_type'] === 'api' ? $this->configuration['model'] : $this->configuration['url'];
  }

}

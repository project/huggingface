<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\AutomaticSpeechRecognitionBase;

/**
 * The rules for a text_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_automatic_speech_recoginition_text_long",
 *   title = @Translation("Huggingface Automatic Speech Recognition"),
 *   field_rule = "text_long",
 * )
 */
class AutomaticSpeechRecognitionTextLong extends AutomaticSpeechRecognitionBase implements AiInterpolatorFieldRuleInterface {

}

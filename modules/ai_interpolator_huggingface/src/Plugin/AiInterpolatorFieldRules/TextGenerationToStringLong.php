<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\TextGenerationBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * The rules for a string_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_text_generation_to_string_long",
 *   title = @Translation("Huggingface Text Generation"),
 *   field_rule = "string_long",
 * )
 */
class TextGenerationToStringLong extends TextGenerationBase implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

}

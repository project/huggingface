<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\SummarizationBase;

/**
 * The rules for a text_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_summarization_to_text_long",
 *   title = @Translation("Huggingface Summarization"),
 *   field_rule = "text_long",
 * )
 */
class SummarizationToTextLong extends SummarizationBase implements AiInterpolatorFieldRuleInterface {

}

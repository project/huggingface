<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\SummarizationBase;

/**
 * The rules for a text_with_summary field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_summarization_to_text_with_summary",
 *   title = @Translation("Huggingface Summarization"),
 *   field_rule = "text_with_summary",
 * )
 */
class SummarizationToTextWithSummary extends SummarizationBase implements AiInterpolatorFieldRuleInterface {

}

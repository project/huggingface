<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\AutomaticSpeechRecognitionBase;

/**
 * The rules for a string_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_automatic_speech_recoginition_string_lon",
 *   title = @Translation("Huggingface Automatic Speech Recognition"),
 *   field_rule = "string_long",
 * )
 */
class AutomaticSpeechRecognitionStringLong extends AutomaticSpeechRecognitionBase implements AiInterpolatorFieldRuleInterface {

}

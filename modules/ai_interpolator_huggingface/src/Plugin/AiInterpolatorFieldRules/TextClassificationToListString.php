<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\TextClassificationBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * The rules for a string field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_text_classification_to_list_string",
 *   title = @Translation("Huggingface Text Classification"),
 *   field_rule = "list_string",
 * )
 */
class TextClassificationToListString extends TextClassificationBase implements AiInterpolatorFieldRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    // Transform string to stripped.
    foreach ($values as $key => $value) {
      $values[$key] = strip_tags($value);
    }
    // Then set the value.
    $entity->set($fieldDefinition->getName(), $values);
  }

}

<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\ImageGenerationBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * The rules for an image field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_text_text_to_image",
 *   title = @Translation("Huggingface Text To Image"),
 *   field_rule = "image",
 *   target = "file",
 * )
 */
class TextToImage extends ImageGenerationBase implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

}

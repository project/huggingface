<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\QuestionAnsweringBase;

/**
 * The rules for a text field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_aquestion_answering_to_text",
 *   title = @Translation("Huggingface Question Answering"),
 *   field_rule = "text",
 * )
 */
class QuestionAnsweringToText extends QuestionAnsweringBase implements AiInterpolatorFieldRuleInterface {

}

<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\SummarizationBase;

/**
 * The rules for a string_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_summarization_to_string_long",
 *   title = @Translation("Huggingface Summarization"),
 *   field_rule = "string_long",
 * )
 */
class SummarizationToStringLong extends SummarizationBase implements AiInterpolatorFieldRuleInterface {

}

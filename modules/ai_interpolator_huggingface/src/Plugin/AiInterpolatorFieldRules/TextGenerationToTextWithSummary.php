<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\TextGenerationBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * The rules for a text_with_summary field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_text_generation_to_text_with_summary",
 *   title = @Translation("Huggingface Text Generation"),
 *   field_rule = "text_with_summary",
 * )
 */
class TextGenerationToTextWithSummary extends TextGenerationBase implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    // Then set the value.
    $entity->set($fieldDefinition->getName(), $values);
  }

}

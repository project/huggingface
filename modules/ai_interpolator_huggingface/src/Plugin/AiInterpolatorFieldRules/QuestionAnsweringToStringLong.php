<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\QuestionAnsweringBase;

/**
 * The rules for a string_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_aquestion_answering_to_string_long",
 *   title = @Translation("Huggingface Question Answering"),
 *   field_rule = "string_long",
 * )
 */
class QuestionAnsweringToStringLong extends QuestionAnsweringBase implements AiInterpolatorFieldRuleInterface {

}

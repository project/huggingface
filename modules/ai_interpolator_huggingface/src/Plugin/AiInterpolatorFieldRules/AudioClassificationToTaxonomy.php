<?php

namespace Drupal\ai_interpolator_huggingface\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_huggingface\AudioClassificationBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * The rules for a taxonomy field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_huggingface_audio_classification_to_taxonomy",
 *   title = @Translation("Huggingface Audio Classification"),
 *   field_rule = "entity_reference",
 *   target = "taxonomy_term"
 * )
 */
class AudioClassificationToTaxonomy extends AudioClassificationBase implements AiInterpolatorFieldRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    $keys = array_keys($config['allowed_values']);

    $realValues = [];
    // If it's not in the keys, go through values.
    foreach ($values as $value) {
      $realValue = '';
      if (!in_array($value, $keys)) {
        foreach ($config['allowed_values'] as $key => $name) {
          if ($value == $name) {
            $realValue = $key;
          }
        }
      }
      else {
        $realValue = $value;
      }
      $realValues[] = $realValue;
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $realValues);
    return TRUE;
  }

}

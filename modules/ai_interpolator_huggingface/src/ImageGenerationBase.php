<?php

namespace Drupal\ai_interpolator_huggingface;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepository;
use Drupal\huggingface\HuggingfaceApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Image Generation base.
 */
class ImageGenerationBase extends HuggingfaceBase implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file repository.
   *
   * @var \Drupal\Core\File\FileRepository
   */
  protected $fileRepo;

  /**
   * The token.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Constructs a new ImageGenerationBase object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, HuggingfaceApi $api, FileSystemInterface $fileSystem, FileRepository $fileRepo, Token $token) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $api);
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->token = $token;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('huggingface.api'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token')
    );
  }

  /**
   * {@inheritDoc}
   */
  public $title = 'Huggingface Text To Image';

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "{{ context }}, realistic";
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = parent::extraAdvancedFormFields($entity, $fieldDefinition);

    $form['interpolator_huggingface_model']['#description'] = $this->t('The full namespace to the model. For instance openai/whisper-large-v3. It has to be an text to image model.');
    $form['interpolator_huggingface_model']['#autocomplete_route_parameters'] = [
      'model_type' => 'text-to-image',
    ];

    $form['interpolator_huggingface_type'] = [
      '#type' => 'value',
      '#default_value' => 'text-to-image',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    $prompts = parent::generate($entity, $fieldDefinition, $interpolatorConfig);
    foreach ($prompts as $prompt) {
      $return = $this->api->textToImage($this->getEndpoint($interpolatorConfig), $prompt);
      if ($return) {
        $values[] = $return;
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Check if the value is set.
    if ($value) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $fileEntities = [];
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    foreach ($values as $value) {
      // Everything validated, then we prepare the file path to save to.
      $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/')) . '/HuggingFace.jpg';
      $file = $this->generateFileFromString($value, $filePath);
      // If we can save, we attach it.
      if ($file) {
        // Get resolution.
        $resolution = getimagesize($file->uri->value);
        // Add to the entities list.
        $fileEntities[] = [
          'target_id' => $file->id(),
          'alt' => $config['default_image']['alt'] ?? '',
          'title' => $config['default_image']['title'] ?? '',
          'width' => $resolution[0],
          'height' => $resolution[1],
        ];
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $fileEntities);
  }

  /**
   * Generate a file entity.
   *
   * @param string $binary
   *   The source binary.
   * @param string $dest
   *   The destination.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file or false on failure.
   */
  private function generateFileFromString(string $binary, string $dest) {
    $path = substr($dest, 0, - (strlen($dest) + 1));
    // Create directory if not existsing.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = $this->fileRepo->writeData($binary, $dest, FileSystemInterface::EXISTS_RENAME);
    if ($file) {
      return $file;
    }
    return FALSE;
  }
}

<?php

namespace Drupal\ai_interpolator_huggingface;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Text classification base.
 */
class TextClassificationBase extends HuggingfaceBase implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Huggingface Text Classification';

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "{{ context }}";
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = parent::extraAdvancedFormFields($entity, $fieldDefinition);

    $form['interpolator_huggingface_model']['#description'] = $this->t('The full namespace to the model. For instance SamLowe/roberta-base-go_emotions. It has to be a text classification model.');
    $form['interpolator_huggingface_model']['#autocomplete_route_parameters'] = [
      'model_type' => 'text-classification',
    ];

    $form['interpolator_huggingface_type'] = [
      '#type' => 'value',
      '#default_value' => 'text-classification',
    ];

    $form['interpolator_huggingface_threshold'] = [
      '#type' => 'textfield',
      '#title' => 'Threshold',
      '#required' => TRUE,
      '#description' => $this->t('The threshold that has to be reached to be filled in.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_threshold', '0.12'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    $prompts = parent::generate($entity, $fieldDefinition, $interpolatorConfig);
    foreach ($prompts as $prompt) {
      $return = json_decode($this->api->textClassification($this->getEndpoint($interpolatorConfig), $prompt), TRUE);
      if (is_array($return[0])) {
        foreach ($return[0] as $result) {
          if ($result['score'] >= $interpolatorConfig['huggingface_threshold']) {
            $values[] = $result['label'];
          }
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Should be a string.
    if (!is_string($value)) {
      return FALSE;
    }
    // Otherwise it is ok.
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    // Transform string to stripped.
    foreach ($values as $key => $value) {
      $values[$key] = strip_tags($value);
    }
    // Then set the value.
    $entity->set($fieldDefinition->getName(), $values);
  }

}

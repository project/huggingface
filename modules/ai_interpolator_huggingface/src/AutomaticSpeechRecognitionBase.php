<?php

namespace Drupal\ai_interpolator_huggingface;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Automatic Speech Recognition base.
 */
class AutomaticSpeechRecognitionBase extends HuggingfaceBase implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Huggingface Automatic Speech Recognition';

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return ['file'];
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = parent::extraAdvancedFormFields($entity, $fieldDefinition);

    $form['interpolator_huggingface_model']['#description'] = $this->t('The full namespace to the model. For instance openai/whisper-large-v3. It has to be an automatic speech recognition model.');
    $form['interpolator_huggingface_model']['#autocomplete_route_parameters'] = [
      'model_type' => 'automatic-speech-recognition',
    ];

    $form['interpolator_huggingface_type'] = [
      '#type' => 'value',
      '#default_value' => 'automatic-speech-recognition',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $target) {
      if ($target->entity) {
        $return = json_decode($this->api->automaticSpeechRecognition($this->getEndpoint($interpolatorConfig), $target->entity->getFileUri()), TRUE);
        if (!empty($return['text'])) {
          $values[] = $return['text'];
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Should be a string.
    if (!is_string($value)) {
      return FALSE;
    }
    // Otherwise it is ok.
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    // Then set the value.
    $entity->set($fieldDefinition->getName(), $values);
  }

}

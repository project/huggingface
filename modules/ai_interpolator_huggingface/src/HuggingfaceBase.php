<?php

namespace Drupal\ai_interpolator_huggingface;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\huggingface\HuggingfaceApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base for Huggingface base.
 */
class HuggingfaceBase extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Huggingface Base';

  /**
   * The huggingface api.
   */
  protected HuggingfaceApi $api;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\huggingface\HuggingfaceApi $api
   *   The huggingface api.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    HuggingfaceApi $api
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->api = $api;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('huggingface.api'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {

    $form['interpolator_huggingface_inference'] = [
      '#type' => 'select',
      '#title' => 'Type of inference',
      '#description' => $this->t('Choose the type of inference to use. Note that the API is rate limited, even if its free and note that some models does not allow both types.'),
      '#options' => [
        'api' => $this->t('API Inference (free)'),
        'dedicated' => $this->t('Dedicated Inference (paid)'),
      ],
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_inference', 'gpt-3.5-turbo'),
      '#weight' => 23,
    ];

    $form['interpolator_huggingface_model'] = [
      '#type' => 'textfield',
      '#title' => 'Huggingface Model',
      '#description' => $this->t('The full namespace to the model. For instance mistralai/Mistral-7B-v0.1'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_model', ''),
      '#weight' => 24,
      '#autocomplete_route_name' => 'huggingface.autocomplete.models',
      '#states' => [
        'visible' => [
          ':input[name="interpolator_huggingface_inference"]' => [
            'value' => 'api',
          ],
        ],
      ],
    ];

    $form['interpolator_huggingface_url'] = [
      '#type' => 'textfield',
      '#title' => 'Huggingface Endpoint URL',
      '#description' => $this->t('The full endpoint URL to the dedicated service.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_url', ''),
      '#weight' => 24,
      '#states' => [
        'visible' => [
          ':input[name="interpolator_huggingface_inference"]' => [
            'value' => 'dedicated',
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Helper function to get endpoint.
   *
   * @param array $interpolatorConfig
   *   The interpolator config.
   *
   * @return string
   *   The endpoint.
   */
  protected function getEndpoint(array $interpolatorConfig) {
    return $interpolatorConfig['huggingface_inference'] == 'api' ? $interpolatorConfig['huggingface_model'] : $interpolatorConfig['huggingface_url'];
  }

  /**
   * Validation endpoint.
   */
  public function validateConfigValues($form, FormStateInterface $formState) {
    if ($formState->getValue('interpolator_enabled') == 1 && $formState->getValue('interpolator_huggingface_inference') == 'api') {
      if ($formState->getValue('interpolator_huggingface_model') === "") {
        $formState->setErrorByName('interpolator_huggingface_model', $this->t('The model is required.'));
      }
      // Check that the model exists.
      $result = json_decode($this->api->getModel($formState->getValue('interpolator_huggingface_model')), TRUE);
      if (!isset($result['id'])) {
        $formState->setErrorByName('interpolator_huggingface_model', $this->t('The model does not exist.'));
      }
      if ($formState->getValue('interpolator_huggingface_type') !== "") {
        if (!isset($result['pipeline_tag']) || $result['pipeline_tag'] != $formState->getValue('interpolator_huggingface_type')) {
          $formState->setErrorByName('interpolator_huggingface_type', $this->t('The model is not a :type model.', [
            ':type' => $formState->getValue('interpolator_huggingface_type'),
          ]));
        }
      }
    }
  }

}

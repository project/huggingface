<?php

namespace Drupal\ai_interpolator_huggingface;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * The text generation base.
 */
class TextGenerationBase extends HuggingfaceBase implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Huggingface Text Generation';

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "This is the text:\n----------------------------------------\n{{ context }}\n----------------------------------------\n\n\nThe idea of the text is\n";
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = parent::extraAdvancedFormFields($entity, $fieldDefinition);

    $form['interpolator_huggingface_model']['#description'] = $this->t('The full namespace to the model. For instance mistralai/Mistral-7B-v0.1. It has to be a text generation model.');
    $form['interpolator_huggingface_model']['#autocomplete_route_parameters'] = [
      'model_type' => 'text-generation',
    ];

    $form['interpolator_huggingface_type'] = [
      '#type' => 'value',
      '#default_value' => 'text-generation',
    ];

    $form['interpolator_huggingface_top_k'] = [
      '#type' => 'number',
      '#title' => $this->t('Top K'),
      '#description' => $this->t('The number of top labels that will be returned by the pipeline. If the provided number is None or higher than the number of labels available in the model configuration, it will default to the number of labels.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_top_k', NULL),
    ];

    $form['interpolator_huggingface_top_p'] = [
      '#type' => 'number',
      '#title' => $this->t('Top P'),
      '#description' => $this->t('The distribution of probability of common tokens. 1.0 means all, 0.0 means none.'),
      '#max' => 1,
      '#min' => 0,
      '#step' => '0.01',
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_top_p', NULL),
    ];

    $form['interpolator_huggingface_temperature'] = [
      '#type' => 'number',
      '#title' => $this->t('Temperature'),
      '#description' => $this->t('The distribution of creativity and determinism. 1.0 means totally random, 0.0 means same text all the time.'),
      '#max' => 1,
      '#min' => 0,
      '#step' => '0.01',
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_temperature', NULL),
    ];

    $form['interpolator_huggingface_repetition_penalty'] = [
      '#type' => 'number',
      '#title' => $this->t('Repetition Penalty'),
      '#description' => $this->t('The more a token is used within generation the more it is penalized to not be picked in successive generation passes. 0.0 to 100.'),
      '#max' => 100,
      '#min' => 0,
      '#step' => '0.01',
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_repetition_penalty', NULL),
    ];

    $form['interpolator_huggingface_max_new_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Max New Tokens'),
      '#description' => $this->t('The amount of maximum tokens to generate. By default it will use generate default.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_max_new_tokens', NULL),
    ];

    $form['interpolator_huggingface_do_sample'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Do Sample'),
      '#description' => $this->t('If checked, this parameter enables decoding strategies such as multinomial sampling, beam-search multinomial sampling, Top-K sampling and Top-p sampling.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_do_sample', NULL),
    ];

    $form['interpolator_huggingface_return_full_text'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Return Full Text'),
      '#description' => $this->t('If unchecked, the return results will not contain the original query making it easier for prompting.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_return_full_text', FALSE),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $prompts = parent::generate($entity, $fieldDefinition, $interpolatorConfig);

    $parameters = [];
    if ($interpolatorConfig['huggingface_top_k'] !== "") {
      $parameters['top_k'] = (int) $interpolatorConfig['huggingface_top_k'];
    }
    if ($interpolatorConfig['huggingface_top_p'] !== "") {
      $parameters['top_p'] = (float) $interpolatorConfig['huggingface_top_p'];
    }
    if ($interpolatorConfig['huggingface_temperature'] !== "") {
      $parameters['temperature'] = (float) $interpolatorConfig['huggingface_temperature'];
    }
    if ($interpolatorConfig['huggingface_repetition_penalty'] !== "") {
      $parameters['repetition_penalty'] = (float) $interpolatorConfig['huggingface_repetition_penalty'];
    }
    if ($interpolatorConfig['huggingface_max_new_tokens'] > "") {
      $parameters['max_new_tokens'] = (int) $interpolatorConfig['huggingface_max_new_tokens'];
    }
    if ($interpolatorConfig['huggingface_do_sample'] !== "") {
      $parameters['do_sample'] = (bool) $interpolatorConfig['huggingface_do_sample'];
    }
    if ($interpolatorConfig['huggingface_return_full_text'] !== "") {
      $parameters['return_full_text'] = (bool) $interpolatorConfig['huggingface_return_full_text'];
    }

    $total = [];
    // Add to get functional output.
    foreach ($prompts as $prompt) {
      $return = json_decode($this->api->textGeneration($this->getEndpoint($interpolatorConfig), $prompt, $parameters), TRUE);

      if (isset($return[0]['generated_text'])) {
        // Remove original since it exists sometimes.
        $total[] = $return[0]['generated_text'];
      }

    }
    return $total;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Should be a string.
    if (!is_string($value)) {
      return FALSE;
    }
    // Otherwise it is ok.
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    // Transform string to stripped.
    foreach ($values as $key => $value) {
      $values[$key] = strip_tags($value);
    }
    // Then set the value.
    $entity->set($fieldDefinition->getName(), $values);
  }

}

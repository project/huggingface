<?php

namespace Drupal\ai_interpolator_huggingface;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Base for Question Answering.
 */
class QuestionAnsweringBase extends HuggingfaceBase implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Huggingface Question Answering';

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = parent::extraAdvancedFormFields($entity, $fieldDefinition);

    $form['interpolator_huggingface_model']['#description'] = $this->t('The full namespace to the model. For instance deepset/roberta-base-squad2. It has to be a question/answering model.');
    $form['interpolator_huggingface_model']['#autocomplete_route_parameters'] = [
      'model_type' => 'question-answering',
    ];

    $form['interpolator_huggingface_type'] = [
      '#type' => 'value',
      '#default_value' => 'question-answering',
    ];

    $form['interpolator_huggingface_question'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The question'),
      '#description' => $this->t('The question to ask the model.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_question', ''),
    ];

    $form['interpolator_huggingface_threshold'] = [
      '#type' => 'textfield',
      '#title' => 'Threshold',
      '#description' => $this->t('The threshold that has to be reached to assume it can be answered. Otherwise answers "No answer found".'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_threshold', '0.3'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $total = [];
    // Add to get functional output.
    foreach ($entity->{$interpolatorConfig['base_field']} as $target) {
      if ($target->value) {
        $return = json_decode($this->api->questionAnswering($this->getEndpoint($interpolatorConfig), $interpolatorConfig['huggingface_question'], strip_tags($target->value)), TRUE);
        $total[] = $return['score'] >= $interpolatorConfig['huggingface_threshold'] ? $return['answer'] : 'No answer found';
      }

    }
    return $total;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Should be a string.
    if (!is_string($value)) {
      return FALSE;
    }
    // Otherwise it is ok.
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    // Then set the value.
    $entity->set($fieldDefinition->getName(), $values);
  }

}

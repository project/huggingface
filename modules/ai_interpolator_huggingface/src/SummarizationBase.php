<?php

namespace Drupal\ai_interpolator_huggingface;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Summarization base.
 */
class SummarizationBase extends HuggingfaceBase implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Huggingface Summarization';

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "{{ context }}";
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form = parent::extraAdvancedFormFields($entity, $fieldDefinition);

    $form['interpolator_huggingface_model']['#description'] = $this->t('The full namespace to the model. For instance mistralai/Mistral-7B-v0.1. It has to be a summarization model.');
    $form['interpolator_huggingface_model']['#description'] = $this->t('The full namespace to the model. For instance sshleifer/distilbart-cnn-12-6. It has to be a summarization model.');
    $form['interpolator_huggingface_model']['#autocomplete_route_parameters'] = [
      'model_type' => 'summarization',
    ];

    $form['interpolator_huggingface_type'] = [
      '#type' => 'value',
      '#default_value' => 'summarization',
    ];

    $form['interpolator_huggingface_top_k'] = [
      '#type' => 'number',
      '#title' => $this->t('Top K'),
      '#description' => $this->t('The number of top labels that will be returned by the pipeline. If the provided number is None or higher than the number of labels available in the model configuration, it will default to the number of labels.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_top_k', NULL),
    ];

    $form['interpolator_huggingface_top_p'] = [
      '#type' => 'number',
      '#title' => $this->t('Top P'),
      '#description' => $this->t('The distribution of probability of common tokens. 1.0 means all, 0.0 means none.'),
      '#max' => 1,
      '#min' => 0,
      '#step' => '0.01',
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_top_p', NULL),
    ];

    $form['interpolator_huggingface_temperature'] = [
      '#type' => 'number',
      '#title' => $this->t('Temperature'),
      '#description' => $this->t('The distribution of creativity and determinism. 1.0 means totally random, 0.0 means same text all the time.'),
      '#max' => 1,
      '#min' => 0,
      '#step' => '0.01',
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_temperature', NULL),
    ];

    $form['interpolator_huggingface_min_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Min Tokens'),
      '#description' => $this->t('The amount of minimum tokens to generate. By default it will use summarization default.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_min_tokens', NULL),
    ];

    $form['interpolator_huggingface_max_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Tokens'),
      '#description' => $this->t('The amount of maximum tokens to generate. By default it will use summarization default.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_max_tokens', NULL),
    ];

    $form['interpolator_huggingface_repetition_penalty'] = [
      '#type' => 'number',
      '#title' => $this->t('Repetition Penalty'),
      '#description' => $this->t('The more a token is used within generation the more it is penalized to not be picked in successive generation passes.'),
      '#max' => 100,
      '#min' => 0,
      '#step' => '0.01',
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_huggingface_repetition_penalty', NULL),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $prompts = parent::generate($entity, $fieldDefinition, $interpolatorConfig);

    $parameters = [];
    if ($interpolatorConfig['huggingface_top_k'] !== "") {
      $parameters['top_k'] = (int) $interpolatorConfig['huggingface_top_k'];
    }
    if ($interpolatorConfig['huggingface_top_p'] !== "") {
      $parameters['top_p'] = (float) $interpolatorConfig['huggingface_top_p'];
    }
    if ($interpolatorConfig['huggingface_temperature'] !== "") {
      $parameters['temperature'] = (float) $interpolatorConfig['huggingface_temperature'];
    }
    if ($interpolatorConfig['huggingface_min_tokens'] !== "") {
      $parameters['min_tokens'] = (int) $interpolatorConfig['huggingface_min_tokens'];
    }
    if ($interpolatorConfig['huggingface_max_tokens'] !== "") {
      $parameters['max_tokens'] = (int) $interpolatorConfig['huggingface_max_tokens'];
    }
    if ($interpolatorConfig['huggingface_repetition_penalty'] !== "") {
      $parameters['repetition_penalty'] = (float) $interpolatorConfig['huggingface_repetition_penalty'];
    }

    $total = [];
    // Add to get functional output.
    foreach ($prompts as $prompt) {
      $return = json_decode($this->api->summarization($this->getEndpoint($interpolatorConfig), $prompt, $parameters), TRUE);

      if (isset($return[0]['summary_text'])) {
        $total[] = $return[0]['summary_text'];
      }

    }
    return $total;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Should be a string.
    if (!is_string($value)) {
      return FALSE;
    }
    // Otherwise it is ok.
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    // Transform string to stripped.
    foreach ($values as $key => $value) {
      $values[$key] = strip_tags($value);
    }
    // Then set the value.
    $entity->set($fieldDefinition->getName(), $values);
  }

}

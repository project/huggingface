<?php

namespace Drupal\runpod_finetune\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Runpod API access.
 */
class RunpodConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   */
  const CONFIG_NAME = 'runpod.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'runpod_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);

    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Runpod API Token'),
      '#description' => $this->t('Can be found and generated <a href="https://www.runpod.io/console/user/settings" target="_blank">here</a>.'),
      '#default_value' => $config->get('api_token'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::CONFIG_NAME)
      ->set('api_token', $form_state->getValue('api_token'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\runpod_finetune\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\huggingface\HuggingfaceApi;
use Drupal\runpod_finetune\Batch\DreamboothFinetuneBatchJobs;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class DreamboothAdjustImage.
 */
class DreamboothAdjustImage extends FormBase implements ContainerInjectionInterface {

  public static $baseModels = [
    'CompVis/stable-diffusion-v1-4' => [
      'name' => 'Stable Diffusion V1.4',
      'resolution' => '512x512',
    ],
    'stabilityai/stable-diffusion-2' => [
      'name' => 'Stable Diffusion 2',
      'resolution' => '768x768',
    ],
  ];

  /**
   * The Huggingface API.
   *
   * @var \Drupal\huggingface\HuggingfaceApi
   */
  protected $huggingfaceApi;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new DreamboothFinetuner object.
   *
   * @param \Drupal\huggingface\HuggingfaceApi $huggingfaceApi
   *   The Huggingface API.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(HuggingfaceApi $huggingfaceApi, StateInterface $state) {
    $this->huggingfaceApi = $huggingfaceApi;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
    return new static(
      $container->get('huggingface.api'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dreambooth_finetuner';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Check if there is a job already.
    $state = $this->state->get(DreamboothFinetuner::$stateKey);
    if (empty($state)) {
      // Redirect back to the first step.
      return new RedirectResponse('/admin/config/dreambooth/generate');
    }

    $form['#attached']['library'][] = 'runpod_finetune/adjust_image';

    foreach ($state['files'] as $fileId) {
      $fileObject = File::load($fileId);
      $form['files'][] = [
        '#type' => 'image_crop',
        '#file' => $fileObject,
        '#prefix' => '<div class="dreambooth-col">',
        '#suffix' => '</div>',
        '#crop_type_list' => ['dreambooth_preparation'],
        '#crop_types_required' => ['dreambooth_preparation'],
        '#show_default_crop' => TRUE,
        '#show_crop_area' => TRUE,
        '#required' => TRUE,
        '#warn_mupltiple_usages' => FALSE,
      ];
    }

    $form['validation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I confirm that I understand that this will cause costs for me and that the developer of the open source project is not responsible if you forget to clean up machines manually if the job fails.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start finetuning'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $file) {
      if (is_array($file) && isset($file['crop_wrapper']) && !$file['crop_wrapper']['dreambooth_preparation']['crop_container']['values']['crop_applied']) {
        $form_state->setErrorByName('files', $this->t('You need to crop all the images.'));
      }
    }
    if (!$form_state->getValue('validation')) {
      $form_state->setErrorByName('validation', $this->t('You need to confirm that you understand the costs.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Check if there is a job already.
    $state = $this->state->get(DreamboothFinetuner::$stateKey);
    if ($state === NULL) {
      // Redirect back to the first step.
      $form_state->setRedirect('runpod_finetune.finetuner');
    }
    // Prepare images - image style overkill.
    $images = [];
    foreach ($form_state->getValues() as $file) {
      if (is_array($file) && isset($file['crop_wrapper']) && $file['crop_wrapper']['dreambooth_preparation']['crop_container']['values']['crop_applied']) {
        // Create a PNG image using GD and crop it according to the crop settings.
        $images[] = $this->cropAndResize($file);
      }
    }
    // Save the actual images as well.
    $state['images'] = $images;
    $this->state->set(DreamboothFinetuner::$stateKey, $state);

    // Setup a batch job that will setup the machines and start the job.
    $batch = [
      'title' => $this->t('Finetuning ' . $state['model_name']),
      'operations' => [
        [
          [DreamboothFinetuneBatchJobs::class, 'prepareSSH'],
          [],
        ],
        [
          [DreamboothFinetuneBatchJobs::class, 'startMachine'],
          [],
        ],
        [
          [DreamboothFinetuneBatchJobs::class, 'pollMachine'],
          [],
        ],
        [
          [DreamboothFinetuneBatchJobs::class, 'connectMachine'],
          [],
        ],
        [
          [DreamboothFinetuneBatchJobs::class, 'uploadData'],
          [],
        ],
        [
          [DreamboothFinetuneBatchJobs::class, 'runFinetuning'],
          [],
        ],
        [
          [DreamboothFinetuneBatchJobs::class, 'checkFinetuning'],
          [],
        ],
        [
          [DreamboothFinetuneBatchJobs::class, 'setupHuggingface'],
          [],
        ],
        [
          [DreamboothFinetuneBatchJobs::class, 'createRepo'],
          [],
        ],
        [
          [DreamboothFinetuneBatchJobs::class, 'uploadRepo'],
          [],
        ],
        [
          [DreamboothFinetuneBatchJobs::class, 'checkUploadStatus'],
          [],
        ],
      ],
      'finished' => [DreamboothFinetuneBatchJobs::class, 'cleanup'],
    ];
    batch_set($batch);
  }

  /**
   * Crop and resize an image.
   *
   * @param array $file
   *   The file array.
   *
   * @return string
   *   The URI of the cropped and resized image.
   */
  protected function cropAndResize($file) {
    $state = $this->state->get(DreamboothFinetuner::$stateKey);
    // Create a PNG image using GD and crop it according to the crop settings.
    $image = imagecreatefromstring(file_get_contents($file['file-uri']));
    $crop = $file['crop_wrapper']['dreambooth_preparation']['crop_container']['values'];
    $image = imagecrop($image, [
      'x' => $crop['x'],
      'y' => $crop['y'],
      'width' => $crop['width'],
      'height' => $crop['height'],
    ]);
    // Resize to the model resolution.
    $resolution = DreamboothFinetuner::$baseModels[$state['base_model']]['resolution'];
    $image = imagescale($image, explode('x', $resolution)[0], explode('x', $resolution)[1]);
    // Save the image to a tmp storage.
    $tmpFile = tempnam(sys_get_temp_dir(), 'dreambooth_') . '.png';
    imagepng($image, $tmpFile);
    $imageUri = $tmpFile;
    imagedestroy($image);
    return $imageUri;
  }

}

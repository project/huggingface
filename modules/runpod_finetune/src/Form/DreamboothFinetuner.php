<?php

namespace Drupal\runpod_finetune\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\huggingface\HuggingfaceApi;

/**
 * Class DreamboothFinetuner.
 */
class DreamboothFinetuner extends FormBase implements ContainerInjectionInterface {

  public static $baseModels = [
    'CompVis/stable-diffusion-v1-4' => [
      'name' => 'Stable Diffusion V1.4',
      'resolution' => '512x512',
    ],
    'stabilityai/stable-diffusion-2' => [
      'name' => 'Stable Diffusion 2',
      'resolution' => '768x768',
    ],
  ];

  public static $stateKey = 'dreambooth_finetune_job';

  /**
   * The Huggingface API.
   *
   * @var \Drupal\huggingface\HuggingfaceApi
   */
  protected $huggingfaceApi;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new DreamboothFinetuner object.
   *
   * @param \Drupal\huggingface\HuggingfaceApi $huggingfaceApi
   *   The Huggingface API.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(HuggingfaceApi $huggingfaceApi, StateInterface $state) {
    $this->huggingfaceApi = $huggingfaceApi;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
    return new static(
      $container->get('huggingface.api'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dreambooth_finetuner';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Check if the API is set.
    if (!$this->huggingfaceApi->isApiSet()) {
      $form['message'] = [
        '#markup' => $this->t('Please configure the Huggingface API settings first.'),
      ];
      return $form;
    }

    // Check if there is a job already.
    if ($this->state->get(self::$stateKey) !== NULL) {
      $form['message'] = [
        '#markup' => $this->t('There is a job already, please cancel that if you want to continue.'),
      ];
      $form['cancel'] = [
        '#type' => 'submit',
        '#value' => $this->t('Cancel job'),
        '#submit' => ['::cancelJob'],
      ];

      return $form;
    }

    $user = json_decode($this->huggingfaceApi->getUserData(), TRUE);
    $form['model_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Model name'),
      '#description' => $this->t('The name of the model to finetune.'),
      '#required' => TRUE,
      '#default_value' => $user['name'] . '/',
    ];

    $form['visibility_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Visibility status'),
      '#description' => $this->t('The visibility status of the model you finetune.'),
      '#options' => [
        'public' => $this->t('Public'),
        'private' => $this->t('Private'),
      ],
      '#default_value' => 'private',
      '#required' => TRUE,
    ];

    $baseModels = [];
    foreach (self::$baseModels as $key => $baseModel) {
      $baseModels[$key] = $baseModel['name'] . ' (' . $baseModel['resolution'] . ')';
    }

    $form['base_model'] = [
      '#type' => 'select',
      '#title' => $this->t('Base model'),
      '#description' => $this->t('The base model to use as origin.'),
      '#required' => TRUE,
      '#options' => $baseModels,
      '#default_value' => 'stabilityai/stable-diffusion-2',
    ];

    $form['prompt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instance Prompt'),
      '#description' => $this->t('The prompt that will be used to describe the image in the token space.'),
      '#required' => TRUE,
      '#default_value' => '',
      '#attributes' => [
        'placeholder' => 'A photo of the person drupalmagoohero.',
      ],
    ];

    $form['files'] = [
      '#title' => $this->t('Training images'),
      '#type' => 'managed_file',
      '#title' => $this->t('Upload a file'),
      '#description' => $this->t('Upload images to use as training data.'),
      '#upload_location' => 'public://',
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => [
          'jpg png jpeg webp gif',
        ],
        'file_validate_image_resolution' => [
          NULL,
          '768x768',
        ],
      ],
    ];

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#open' => FALSE,
    ];

    $form['advanced']['runpod_machine'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Runpod Machine ID'),
      '#description' => $this->t('The ID of the Runpod machine to use.'),
      '#default_value' => 'NVIDIA A40',
      '#required' => TRUE,
    ];

    $form['advanced']['train_batch_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Train batch size'),
      '#description' => $this->t('Batch size (per device) for the training dataloader.'),
      '#default_value' => 4,
      '#min' => 1,
      '#max' => 100,
      '#step' => 1,
      '#required' => TRUE,
    ];

    $form['advanced']['gradient_accumulation_steps'] = [
      '#type' => 'number',
      '#title' => $this->t('Gradient Accumulation Steps'),
      '#description' => $this->t('Number of updates steps to accumulate before performing a backward/update pass.'),
      '#default_value' => 1,
      '#min' => 1,
      '#max' => 100,
      '#step' => 1,
      '#required' => TRUE,
    ];

    $form['advanced']['learning_rate'] = [
      '#type' => 'number',
      '#title' => $this->t('Learning rate'),
      '#description' => $this->t('The learning rate for the optimizer.'),
      '#default_value' => 5e-6,
      '#min' => 1e-6,
      '#max' => 1e-1,
      '#step' => 1e-6,
      '#required' => TRUE,
    ];

    $form['advanced']['lr_scheduler'] = [
      '#type' => 'select',
      '#title' => $this->t('Learning Rate Scheduler'),
      '#description' => $this->t('The scheduler type to use.'),
      '#default_value' => 'constant',
      '#options' => [
        'constant' => $this->t('Constant'),
        'linear' => $this->t('Linear'),
        'cosine' => $this->t('Cosine'),
        'cosine_with_restarts' => $this->t('Cosine with restarts'),
        'polynomial' => $this->t('Polynomial'),
        'constant_with_warmup' => $this->t('Constant with warmup'),
      ],
      '#required' => TRUE,
    ];

    $form['advanced']['lr_warmup_steps'] = [
      '#type' => 'number',
      '#title' => $this->t('Learning Rate Warmup Steps'),
      '#description' => $this->t('Number of steps for the warmup in the lr scheduler.'),
      '#default_value' => 10,
      '#min' => 0,
      '#step' => 1,
      '#required' => TRUE,
    ];

    $form['advanced']['lr_power'] = [
      '#type' => 'number',
      '#title' => $this->t('Learning Rate Power Factor'),
      '#description' => $this->t('Power factor of the polynomial scheduler."'),
      '#default_value' => 1.0,
      '#step' => 0.1,
      '#required' => TRUE,
    ];

    $form['advanced']['max_train_steps'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Train Steps'),
      '#description' => $this->t('Total number of training steps to perform.'),
      '#default_value' => 500,
      '#step' => 1,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Go to adjusting image'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check what triggered the submit.
    $trigger = $form_state->getTriggeringElement()['#id'];
    if ($trigger !== 'edit-cancel' && count($form_state->getValue('files')) < 10) {
      $form_state->setErrorByName('files', $this->t('You need to upload at least 10 images.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->set(self::$stateKey, [
      'model_name' => $form_state->getValue('model_name'),
      'visibility_status' => $form_state->getValue('visibility_status'),
      'base_model' => $form_state->getValue('base_model'),
      'prompt' => $form_state->getValue('prompt'),
      'files' => $form_state->getValue('files'),
      'resolution' => explode('x', self::$baseModels[$form_state->getValue('base_model')]['resolution'])[0],
      'advanced' => [
        'runpod_machine' => $form_state->getValue('runpod_machine'),
        'train_batch_size' => $form_state->getValue('train_batch_size'),
        'gradient_accumulation_steps' => $form_state->getValue('gradient_accumulation_steps'),
        'learning_rate' => $form_state->getValue('learning_rate'),
        'lr_scheduler' => $form_state->getValue('lr_scheduler'),
        'lr_warmup_steps' => $form_state->getValue('lr_warmup_steps'),
        'lr_power' => $form_state->getValue('lr_power'),
        'max_train_steps' => $form_state->getValue('max_train_steps'),
      ],
    ]);
    $trigger = $form_state->getTriggeringElement()['#id'];
    if ($trigger !== 'edit-cancel') {
      $form_state->setRedirect('runpod_finetune.dreambooth_adjust_image');
    }
  }

  /**
   * Cancel the job.
   */
  public function cancelJob(array &$form, FormStateInterface $form_state) {
    $this->state->delete(self::$stateKey);
  }

}

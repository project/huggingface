<?php

namespace Drupal\runpod_finetune;

use Drupal\Core\Config\ConfigFactory;
use Drupal\runpod_finetune\Form\RunpodConfigForm;
use GuzzleHttp\Client;

/**
 * Basic Runpod API.
 */
class RunpodApi {

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * API Token.
   */
  private string $apiToken;

  /**
   * The graphql base path.
   */
  private string $graphql = 'https://api.runpod.io/graphql';

  /**
   * Constructs a new Huggingface object.
   *
   * @param \GuzzleHttp\Client $client
   *   Http client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(Client $client, ConfigFactory $configFactory) {
    $this->client = $client;
    $config = $configFactory->get(RunpodConfigForm::CONFIG_NAME);
    $this->apiToken = $config->get('api_token') ?? '';
  }

  /**
   * Creates a pod.
   *
   * @param string $name
   *   The name of the pod.
   * @param string $imageName
   *   The image to use.
   * @param string $gpuTypeId
   *   The GPU type to use.
   *
   * @return string|object
   *   The response.
   */
  public function createOnDemandPodFromImage($name, $imageName, $gpuTypeId, array $options = []) {
    $gpuCount = $options['gpuCount'] ?? 1;
    $volumeInGb = $options['volumeInGb'] ?? 20;
    $containerDiskInGb = $options['containerDiskInGb'] ?? 20;
    $minVcpuCount = $options['minVcpuCount'] ?? 2;
    $minMemoryInGb = $options['minMemoryInGb'] ?? 15;
    $dockerArgs = $options['dockerArgs'] ?? '';
    $ports = $options['ports'] ?? '';
    $volumeMountPath = $options['volumeMountPath'] ?? '/workspace';
    $env = "[]";
    if ($options['env'] && is_array($options['env'])) {
      // Set key and value in string.
      $env = "[";
      foreach ($options['env'] as $key => $data) {
        $env .= "{key: \"$data[key]\", value: \"$data[value]\"}";
        if ($key != count($options['env']) - 1) {
          $env .= ',';
        }
      }
      $env .= "]";
    }

    $string = <<<GRAPHQL
      mutation {
        podFindAndDeployOnDemand(
          input: {
            cloudType: ALL,
            gpuCount: $gpuCount,
            volumeInGb: $volumeInGb,
            containerDiskInGb: $containerDiskInGb,
            minVcpuCount: $minVcpuCount,
            minMemoryInGb: $minMemoryInGb,
            gpuTypeId: "$gpuTypeId",
            name: "$name",
            imageName: "$imageName",
            dockerArgs: "$dockerArgs",
            ports: "$ports",
            volumeMountPath: "$volumeMountPath",
            env: $env
          }
        )
        { id imageName env machineId machine { podHostId } }
      }
    GRAPHQL;
    $response = $this->makeRequest($string);

    return $response;
  }

  /**
   * Creates a pod.
   *
   * @param string $name
   *   The name of the pod.
   * @param string $templateId
   *   The template id to use.
   * @param string $gpuTypeId
   *   The GPU type to use.
   *
   * @return string|object
   *   The response.
   */
  public function createOnDemandPodFromTempate($name, $templateId, $gpuTypeId, array $options = []) {
    $gpuCount = $options['gpuCount'] ?? 1;
    $volumeInGb = $options['volumeInGb'] ?? 20;
    $containerDiskInGb = $options['containerDiskInGb'] ?? 20;
    $minVcpuCount = $options['minVcpuCount'] ?? 2;
    $minMemoryInGb = $options['minMemoryInGb'] ?? 15;
    $dockerArgs = $options['dockerArgs'] ?? '';
    $ports = $options['ports'] ?? '';
    $volumeMountPath = $options['volumeMountPath'] ?? '/workspace';
    $env = "[]";
    if ($options['env'] && is_array($options['env'])) {
      // Set key and value in string.
      $env = "[";
      foreach ($options['env'] as $key => $data) {
        $env .= "{key: \"$data[key]\", value: \"$data[value]\"}";
        if ($key != count($options['env']) - 1) {
          $env .= ',';
        }
      }
      $env .= "]";
    }

    $string = <<<GRAPHQL
      mutation {
        podFindAndDeployOnDemand(
          input: {
            cloudType: ALL,
            gpuCount: $gpuCount,
            volumeInGb: $volumeInGb,
            containerDiskInGb: $containerDiskInGb,
            minVcpuCount: $minVcpuCount,
            minMemoryInGb: $minMemoryInGb,
            gpuTypeId: "$gpuTypeId",
            name: "$name",
            templateId: "$templateId",
            dockerArgs: "$dockerArgs",
            ports: "$ports",
            volumeMountPath: "$volumeMountPath",
            env: $env
          }
        )
        { id imageName env machineId machine { podHostId } }
      }
    GRAPHQL;
    $response = $this->makeRequest($string);

    return $response;
  }

  /**
   * Query pod.
   *
   * @param string $id
   *   The pod id.
   *
   * @return string|object
   *   The response.
   */
  public function queryPod($id) {
    $string = <<<GRAPHQL
      query Pod {
        pod(
          input: {
            podId: "$id"
          }) {
            id
            name
            runtime {
              uptimeInSeconds
              ports {
                ip
                isIpPublic
                privatePort
                publicPort
                type
              }
              gpus {
                id
                gpuUtilPercent
                memoryUtilPercent
              }
              container {
                cpuPercent
                memoryPercent
              }
            }
          }
        }
    GRAPHQL;
    $response = $this->makeRequest($string);

    return $response;
  }

  /**
   * Stop pod.
   *
   * @param string $id
   *   The pod id.
   *
   * @return string|object
   *   The response.
   */
  public function stopPod($id) {
    $string = <<<GRAPHQL
      mutation {
        podStop(
          input: {
            podId: "$id"
          }
        )
        { id }
      }
    GRAPHQL;
    $response = $this->makeRequest($string);

    return $response;
  }

  /**
   * Terminate pod.
   *
   * @param string $id
   *   The pod id.
   *
   * @return string|object
   *   The response.
   */
  public function terminatePod($id) {
    $string = <<<GRAPHQL
      mutation {
        podTerminate(
          input: {
            podId: "$id"
          }
        )
      }
    GRAPHQL;
    $response = $this->makeRequest($string);

    return $response;
  }

  /**
   * Make Runpod Graphql call.
   *
   * @param string $payload
   *   The graphql payload to make.
   *
   * @return string|object
   *   The return response.
   */
  protected function makeRequest($payload) {
    if (empty($this->apiToken)) {
      throw new \Exception('No Runpod API token found.');
    }

    // We can wait some.
    $options['connect_timeout'] = 120;
    $options['read_timeout'] = 120;
    // Don't fail.
    $options['http_errors'] = FALSE;
    // Set authorization header.
    $options['headers']['Authorization'] = 'Bearer ' . $this->apiToken;

    $options['body'] = json_encode([
      'query' => preg_replace('/\s+/', ' ', str_replace(["\n"], "", trim($payload)))
    ]);

    $options['headers']['Content-Type'] = 'application/json';

    $url = $this->graphql . '?api_key=' . $this->apiToken;

    $res = $this->client->request('POST', $url, $options);
    return $res->getBody();
  }
}

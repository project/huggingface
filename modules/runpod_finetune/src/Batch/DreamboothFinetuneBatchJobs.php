<?php

namespace Drupal\runpod_finetune\Batch;

use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\runpod_finetune\Form\DreamboothFinetuner;
use phpseclib3\Crypt\PublicKeyLoader;
use phpseclib3\Crypt\RSA;
use phpseclib3\Net\SFTP;
use phpseclib3\Net\SSH2;

/**
 * Class DreamboothFinetune.
 */
class DreamboothFinetuneBatchJobs {

  /**
   * The template id to use.
   */
  protected static $templateId = 'jljxc6naq8';

  /**
   * The Runpod name to use.
   */
  protected static $runpodName = 'Drupal Dreambooth Finetune';

  /**
   * Prepare SSH keys.
   */
  public static function prepareSSH(&$context) {
    $context['message'] = t('Preparing SSH keys...');
    $state = self::getState();
    // Generate SSH key.
    $private = RSA::createKey(2048);
    $state['ssh']['private'] = $private->toString('PKCS1');
    $state['ssh']['public'] = $private->getPublicKey()->toString('OpenSSH');

    self::setState($state);
  }

  /**
   * Start the machine.
   */
  public static function startMachine(&$context) {
    $context['message'] = t('Starting a Runpod Machine...');
    $state = self::getState();

    // Set the public key.
    $options['env'][] = [
      'key' => 'EXTRA_PUBLIC_SSH',
      'value' => $state['ssh']['public'],
    ];

    // Open up SSH.
    $options['ports'] = '22/tcp';
    // Make the disk large enough.
    $options['containerDiskInGb'] = 80;
    $runpodId = $state['advanced']['runpod_machine'] ?? 'NVIDIA A40';
    // Ask to start the machine.
    $response = self::getRunpodApi()->createOnDemandPodFromTempate(self::$runpodName, self::$templateId, $runpodId, $options);
    $json = json_decode($response->getContents(), TRUE);
    if (isset($json['data']['podFindAndDeployOnDemand']['id'])) {
      $state['pod_id'] = $json['data']['podFindAndDeployOnDemand']['id'];
      self::setState($state);
    }
    else {
      throw new \Exception('Failed to start the machine, cancelling.');
    }
  }

  /**
   * Poll the machine state.
   */
  public static function pollMachine(&$context) {
    $context['message'] = t('Waiting for the Runpod machine to build and start (might take some time)...');
    $state = self::getState();
    $response = self::getRunpodApi()->queryPod($state['pod_id']);
    $status = json_decode($response->getContents(), TRUE);
    if (empty($status['data']['pod']['runtime']['ports'][0]['ip'])) {
      // Wait 5 seconds before next job.
      sleep(5);
      $context['finished'] = 0;
    }
    else {
      $state['server'] = $status['data']['pod']['runtime'];
      self::setState($state);
    }
  }

  /**
   * Try to connect to the machine.
   */
  public static function connectMachine(&$context) {
    $context['message'] = t('Testing to connect to the machine...');
    if (self::getSSHClient() == NULL) {
      sleep(5);
      $context['finished'] = 0;
    }
  }

  /**
   * Upload the images and config.
   */
  public static function uploadData() {
    $context['message'] = t('Uploading training data...');
    $state = self::getState();
    $sftp = self::getSFTPClient();
    if ($sftp == NULL) {
      throw new \Exception('Failed to connect to the server.');
    }
    // Generate the config.
    $configFilePath = self::generateBashConfig();
    // Upload the config.
    $sftp->put('/dreambooth_config.sh', $configFilePath, SFTP::SOURCE_LOCAL_FILE);
    // Upload the images.
    foreach ($state['images'] as $image) {
      $sftp->put('/training-data/' . basename($image), $image, SFTP::SOURCE_LOCAL_FILE);
    }
  }

  /**
   * Run the finetuning.
   */
  public static function runFinetuning(&$context) {
    $context['message'] = t('Starting finetuning...');
    $ssh = self::getSSHClient();
    if ($ssh == NULL) {
      throw new \Exception('Failed to connect to the server.');
    }
    // Check if the job is already runnnig (someone clicked reload).
    $output = $ssh->exec('ps aux | grep train_dreambooth.py');

    if (!str_contains($output, 'accelerate')) {
      // Start the job in the background
      $ssh->exec('./start.sh');
    }
  }

  /**
   * Check the status of the finetuning.
   */
  public static function checkFinetuning(&$context) {
    // Start the job in the background
    $context['message'] = t('Checking if finetuning is finished (This will take long time)...');
    $ssh = self::getSSHClient();
    if ($ssh == NULL) {
      throw new \Exception('Failed to connect to the server.');
    }
    // Check if the job is still running.
    $output = $ssh->exec('ps aux | grep train_dreambooth.py');
    if (str_contains($output, 'accelerate')) {
      sleep(5);
      $context['finished'] = 0;
    }
  }

  /**
   * Setup hugginface on the server.
   */
  public static function setupHuggingface(&$context) {
    $state = self::getState();
    $context['message'] = t('Setting up Huggingface...');
    $ssh = self::getSSHClient();
    if ($ssh == NULL) {
      throw new \Exception('Failed to connect to the server.');
    }
    // Setup the huggingface.
    $ssh->exec('git config --global credential.helper store');
    // Clean up the model.
    $ssh->exec('rm -rf /new-model/logs');
    $sftp = self::getSFTPClient();
    if ($sftp == NULL) {
      throw new \Exception('Failed to connect to the server.');
    }
    $sftp->put('/root/.cache/huggingface/token', self::getHuggingfaceApiToken(), SFTP::SOURCE_STRING);

    // Create a readme and upload.
    $readme = str_replace([
      '{{ prompt }}' => $state['prompt'],
      '{{ base_model }}' => $state['base_model'],
      '{{ model_name }}' => $state['model_name'],
    ],
    [
    ],
    self::getModuleBasePath() . '/assets/dreambooth_readme.md');
    $sftp->put('/new-model/README.md', $readme, SFTP::SOURCE_STRING);
  }

  /**
   * Create repo.
   */
  public static function createRepo(&$context) {
    $context['message'] = t('Create Repo...');
    $state = self::getState();
    $ssh = self::getSSHClient();
    if ($ssh == NULL) {
      throw new \Exception('Failed to connect to the server.');
    }
    $private = $state['visibility_status'] == 'private' ? 'true' : 'false';
    // Create the repo.
    $ssh->exec('/opt/venv/bin/python /create_repo.py ' . $state['model_name'] . ' ' . $private);
  }

  /**
   * Upload repo.
   */
  public static function uploadRepo(&$context) {
    $context['message'] = t('Starting uploading Repo...');
    $state = self::getState();
    $ssh = self::getSSHClient();
    if ($ssh == NULL) {
      throw new \Exception('Failed to connect to the server.');
    }
    // Upload the repo.
    $ssh->exec('screen -dmS upload /opt/venv/bin/huggingface-cli upload ' . $state['model_name'] . ' /new-model .');
  }

  /**
   * Check upload status.
   */
  public static function checkUploadStatus(&$context) {
    $context['message'] = t('Waiting for upload to finish...');
    $ssh = self::getSSHClient();
    if ($ssh == NULL) {
      throw new \Exception('Failed to connect to the server.');
    }
    // Check if the job is still running.
    $output = $ssh->exec('ps aux | grep huggingface-cli');
    if (str_contains($output, 'huggingface-cli upload')) {
      sleep(5);
      $context['finished'] = 0;
    }
  }

  /**
   * Cleanup.
   */
  public static function cleanup() {
    $state = self::getState();
    // Stop machine.
    self::getRunpodApi()->stopPod($state['pod_id']);
    // Delete machine.
    self::getRunpodApi()->terminatePod($state['pod_id']);

    // Delete files.
    foreach ($state['images'] as $image) {
      $file = File::load($image);
      if ($file) {
        $file->delete();
      }
    }
    // Send a message.
    self::getMessenger()->addMessage(t('Finetuning is finished, visit your model here <a href="@link" target="_blank">@link</a>.', [
      '@link' => 'https://huggingface.co/' . $state['model_name'],
    ]));
    // Remove state.
    self::deleteState();
  }

  /**
   * Generate and return a bash config file.
   *
   * @return string
   *   The bash config file path.
   */
  protected static function generateBashConfig() {
    $state = self::getState();
    $config = "#!/bin/bash\n";
    $config .= 'MODEL="' . $state['base_model'] . "\"\n";
    $config .= 'INSTANCE_PROMPT="' . $state['prompt'] . "\"\n";
    $config .= 'RESOLUTION=' . $state['resolution'] . "\n";
    $config .= 'TRAIN_BATCH_SIZE=' . $state['advanced']['train_batch_size'] . "\n";
    $config .= 'GRADIENT_ACCUMULATION_STEPS=' . $state['advanced']['gradient_accumulation_steps'] . "\n";
    $config .= 'LEARNING_RATE=' . $state['advanced']['learning_rate'] . "\n";
    $config .= 'LR_SCHEDULER="' . $state['advanced']['lr_scheduler'] . "\"\n";
    $config .= 'LR_WARMUP_STEPS=' . $state['advanced']['lr_warmup_steps'] . "\n";
    $config .= 'LR_POWER=' . $state['advanced']['lr_power'] . "\n";
    $config .= 'MAX_TRAIN_STEPS=' . $state['advanced']['max_train_steps'] . "\n";
    $config .= 'HUB_MODEL_ID="' . $state['model_name'] . "\"\n";
    $config .= 'HUB_TOKEN="' . self::getHuggingfaceApiToken() . "\"\n";

    // Get the temp direcotry.
    $tmpDir = \Drupal::service('file_system')->getTempDirectory();
    $filePath = $tmpDir . '/dreambooth_config.sh';
    file_put_contents($filePath, $config);
    return $filePath;
  }

  /**
   * Get a SSH client.
   *
   * @return \phpseclib3\Net\SSH2|null
   *   The SSH client.
   */
  protected static function getSSHClient() {
    $state = self::getState();
    $key = PublicKeyLoader::load($state['ssh']['private']);
    // Get the connect data.
    $connect = self::getHost($state['server']['ports']);
    $ssh = new SSH2($connect['host'], $connect['port']);
    return $ssh->login('root', $key) ? $ssh : NULL;
  }

  /**
   * Get a SFTP client.
   *
   * @return \phpseclib3\Net\SFTP|null
   *   The SFTP client.
   */
  protected static function getSFTPClient() {
    $state = self::getState();
    $key = PublicKeyLoader::load($state['ssh']['private']);
    // Get the connect data.
    $connect = self::getHost($state['server']['ports']);
    $sftp = new SFTP($connect['host'], $connect['port']);
    return $sftp->login('root', $key) ? $sftp : NULL;
  }

  /**
   * Helper function to get the normal port 22 host.
   *
   * @param array $ports
   *   The ports.
   *
   * @return array
   *   The host and the actual port.
   */
  protected static function getHost(array $ports) {
    foreach ($ports as $port) {
      if ($port['privatePort'] == 22) {
        return [
          'port' => $port['publicPort'],
          'host' => $port['ip'],
        ];
      }
    }
    return '';
  }

  /**
   * Get the drupal messenger.
   *
   * @return \Drupal\Core\Messenger\MessengerInterface
   *   The messenger.
   */
  protected static function getMessenger() {
    return \Drupal::messenger();
  }

  /**
   * Get the Huggingface API token.
   *
   * @return string
   *   The Huggingface API token.
   */
  protected static function getHuggingfaceApiToken() {
    return \Drupal::config('huggingface.settings')->get('api_token');
  }

  /**
   * Get the runpod api.
   *
   * @return \Drupal\runpod_finetune\RunpodApi
   *   The runpod api.
   */
  protected static function getRunpodApi() {
    return \Drupal::service('runpod_finetune.api');
  }

  /**
   * Delete the state.
   */
  public static function deleteState() {
    \Drupal::state()->delete(DreamboothFinetuner::$stateKey);
  }

  /**
   * Gets the modules base path.
   *
   * @return string
   *   The base path.
   */
  public static function getModuleBasePath() {
    return \Drupal::service('extension.list.module')->get('runpod_finetune')->getPath();
  }

  /**
   * Get the state.
   *
   * @return array
   *   The state.
   */
  protected static function getState() {
    return \Drupal::state()->get(DreamboothFinetuner::$stateKey);
  }

  /**
   * Set the state.
   *
   * @param array $state
   *   The state.
   *
   * @return bool
   *   The state.
   */
  protected static function setState(array $state) {
    return \Drupal::state()->set(DreamboothFinetuner::$stateKey, $state);
  }

}

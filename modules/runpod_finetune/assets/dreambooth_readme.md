---
license: creativeml-openrail-m
library_name: diffusers
tags:
- text-to-image
- dreambooth
- diffusers-training
- stable-diffusion
- stable-diffusion-diffusers
- text-to-image
- dreambooth
- diffusers-training
- stable-diffusion
- stable-diffusion-diffusers
base_model: {{ base_model }}
inference: true
instance_prompt: {{ prompt }}
---

<!-- This model card has been generated automatically according to the information the training script had access to. You
should probably proofread and complete it, then remove this comment. -->


# DreamBooth - {{ model_name }}

This is a dreambooth model derived from {{ base_model }}. The weights were trained on {{ prompt }} using [DreamBooth](https://dreambooth.github.io/).


DreamBooth for the text encoder was enabled: False.

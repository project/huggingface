<?php

namespace Drupal\huggingface\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Huggingface API access.
 */
class HuggingfaceConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   */
  const CONFIG_NAME = 'huggingface.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'huggingface_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);

    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Huggingface API Token'),
      '#description' => $this->t('Can be found and generated <a href="https://huggingface.co/settings/tokens" target="_blank">here</a>.'),
      '#default_value' => $config->get('api_token'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::CONFIG_NAME)
      ->set('api_token', $form_state->getValue('api_token'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
